# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 09:57:54 2015

@author: lucadelu
"""
from qgis.utils import iface
from qgis.gui import QgsMessageBar
from qgis.core import QgsMessageLog
import os
try:
    from PIL import Image
except:
    raise ImportError("PIL library not found, please install it")

IMP_ERROR = 'Python GDAL library not found, please install python-gdal'
try:
    import osgeo.gdal as gdal
except ImportError:
    try:
        import gdal
    except ImportError:
        raise ImportError(IMP_ERROR)


class GPDFMessageHandler:
    """
    Handler of message notification via QgsMessageBar.

    GPDFMessageHandler.[information, warning, critical, success](title, text,
                                                                 timeout)
    GPDFMessageHandler.[information, warning, critical, success](title, text)
    GPDFMessageHandler.error(message)
    """

    messageLevel = [QgsMessageBar.INFO,
                    QgsMessageBar.WARNING,
                    QgsMessageBar.CRITICAL]

    try:
        messageLevel.append(QgsMessageBar.SUCCESS)
    except:
        pass
    messageTime = iface.messageTimeout()

    @staticmethod
    def information(title="", text="", timeout=None):
        """Return an information message"""
        level = GPDFMessageHandler.messageLevel[0]
        if timeout is None:
            timeout = GPDFMessageHandler.messageTime
        GPDFMessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def warning(title="", text="", timeout=None):
        """Return a warning message"""
        level = GPDFMessageHandler.messageLevel[1]
        if timeout is None:
            timeout = GPDFMessageHandler.messageTime
        GPDFMessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def critical(title="", text="", timeout=None):
        """Return a critical message"""
        level = GPDFMessageHandler.messageLevel[2]
        if timeout is None:
            timeout = GPDFMessageHandler.messageTime
        GPDFMessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def success(title="", text="", timeout=None):
        """Return a success message"""
        # SUCCESS was introduced in 2.7
        # if it throws an AttributeError INFO will be used
        try:
            level = GPDFMessageHandler.messageLevel[3]
        except:
            level = GPDFMessageHandler.messageLevel[0]
        if timeout is None:
            timeout = GPDFMessageHandler.messageTime
        GPDFMessageHandler.messageBar(title, text, level, timeout)

    @staticmethod
    def error(message):
        """Return an error message"""
        GPDFMessageHandler.warning("Error",
                                   "Check QGIS log messages", 0)
        QgsMessageLog.logMessage(message, "GeospatialPDF Plugin")

    @staticmethod
    def messageBar(title, text, level, timeout):
        """Set message bar"""
        if title:
            iface.messageBar().pushMessage(title.decode('utf-8'),
                                           text.decode('utf-8'), level,
                                           timeout)
        else:
            iface.messageBar().pushMessage(text.decode('utf-8'), level,
                                           timeout)


class FileInfo:
    """A class holding information about a GDAL file.

       Class copied from gdal_merge.py, extended by Luca Delucchi

       :param str filename: Name of file to read.

       :return: 1 on success or 0 if the file can't be opened.
    """

    def __init__(self, filename):
        """Initialize file_info from filename"""
        self.s_fh = gdal.Open(filename)
        if self.s_fh is None:
            GPDFMessageHandler.error("The file {dire} is recognized by "
                                     "GDAL".format(dire=filename))

        self.bands = self.s_fh.RasterCount
        self.xsize = self.s_fh.RasterXSize
        self.ysize = self.s_fh.RasterYSize
        self.band = self.s_fh.GetRasterBand(1)
        self.band_type = self.band.DataType
        self.block_size = self.band.GetBlockSize()
        self.projection = self.s_fh.GetProjection()
        self.geotransform = self.s_fh.GetGeoTransform()
        self.ulx = self.geotransform[0]
        self.uly = self.geotransform[3]
        self.lrx = self.ulx + self.geotransform[1] * self.xsize
        self.lry = self.uly + self.geotransform[5] * self.ysize

        self.meta = self.s_fh.GetMetadata()
        if '_FillValue' in self.meta.keys():
            self.fill_value = self.meta['_FillValue']
        else:
            self.fill_value = None

        colortable = self.band.GetRasterColorTable()
        if colortable is not None:
            self.colortable = colortable.Clone()
        else:
            self.colortable = None

    def copy_into(self, t_fh, t_band=1, s_band=1, nodata_arg=None):
        """Copy this files image into target file.

        This method will compute the overlap area of the file_info objects
        file, and the target gdal.Dataset object, and copy the image data
        for the common window area.  It is assumed that the files are in
        a compatible projection. no checking or warping is done.  However,
        if the destination file is a different resolution, or different
        image pixel type, the appropriate resampling and conversions will
        be done (using normal GDAL promotion/demotion rules).

        :param t_fh: gdal.Dataset object for the file into which some or all
                     of this file may be copied.
        :param s_band: source band
        :param t_band: target band
        :param nodata_arg:

        :return: 1 on success (or if nothing needs to be copied), and zero one
                 failure.

        """
        t_geotransform = t_fh.GetGeoTransform()
        t_ulx = t_geotransform[0]
        t_uly = t_geotransform[3]
        t_lrx = t_geotransform[0] + t_fh.RasterXSize * t_geotransform[1]
        t_lry = t_geotransform[3] + t_fh.RasterYSize * t_geotransform[5]

        # figure out intersection region
        tgw_ulx = max(t_ulx, self.ulx)
        tgw_lrx = min(t_lrx, self.lrx)
        if t_geotransform[5] < 0:
            tgw_uly = min(t_uly, self.uly)
            tgw_lry = max(t_lry, self.lry)
        else:
            tgw_uly = max(t_uly, self.uly)
            tgw_lry = min(t_lry, self.lry)

        # do they even intersect?
        if tgw_ulx >= tgw_lrx:
            return 1
        if t_geotransform[5] < 0 and tgw_uly <= tgw_lry:
            return 1
        if t_geotransform[5] > 0 and tgw_uly >= tgw_lry:
            return 1

        # compute target window in pixel coordinates.
        tw_xoff = int((tgw_ulx - t_geotransform[0]) / t_geotransform[1] + 0.1)
        tw_yoff = int((tgw_uly - t_geotransform[3]) / t_geotransform[5] + 0.1)
        tw_xsize = int((tgw_lrx - t_geotransform[0]) / t_geotransform[1] +
                       0.5) - tw_xoff
        tw_ysize = int((tgw_lry - t_geotransform[3]) / t_geotransform[5] +
                       0.5) - tw_yoff

        if tw_xsize < 1 or tw_ysize < 1:
            return 1

        # Compute source window in pixel coordinates.
        sw_xoff = int((tgw_ulx - self.geotransform[0]) / self.geotransform[1])
        sw_yoff = int((tgw_uly - self.geotransform[3]) / self.geotransform[5])
        sw_xsize = int((tgw_lrx - self.geotransform[0])
                       / self.geotransform[1] + 0.5) - sw_xoff
        sw_ysize = int((tgw_lry - self.geotransform[3])
                       / self.geotransform[5] + 0.5) - sw_yoff

        if sw_xsize < 1 or sw_ysize < 1:
            return 1

        return raster_copy(self.s_fh, sw_xoff, sw_yoff, sw_xsize, sw_ysize,
                           s_band, t_fh, tw_xoff, tw_yoff, tw_xsize, tw_ysize,
                           t_band, nodata_arg)


# =============================================================================
def raster_copy(s_fh, s_xoff, s_yoff, s_xsize, s_ysize, s_band_n,
                t_fh, t_xoff, t_yoff, t_xsize, t_ysize, t_band_n,
                nodata=None):
    """Copy a band of raster into the output file.

       Function copied from gdal_merge.py
    """
    if nodata is not None:
        return raster_copy_with_nodata(s_fh, s_xoff, s_yoff, s_xsize, s_ysize,
                                       s_band_n, t_fh, t_xoff, t_yoff, t_xsize,
                                       t_ysize, t_band_n, nodata)

    s_band = s_fh.GetRasterBand(s_band_n)
    t_band = t_fh.GetRasterBand(t_band_n)

#    data = s_band.ReadRaster(s_xoff, s_yoff, s_xsize, s_ysize,
#                             t_xsize, t_ysize, t_band.DataType)
#
#    t_band.WriteRaster(t_xoff, t_yoff, t_xsize, t_ysize, data, t_xsize,
#                       t_ysize, t_band.DataType)

    data_src = s_band.ReadAsArray(s_xoff, s_yoff, s_xsize, s_ysize,
                                  t_xsize, t_ysize)
    t_band.WriteArray(data_src, t_xoff, t_yoff)

    s_fh = None
    return 0


def raster_copy_with_nodata(s_fh, s_xoff, s_yoff, s_xsize, s_ysize, s_band_n,
                            t_fh, t_xoff, t_yoff, t_xsize, t_ysize, t_band_n,
                            nodata):
    """Copy a band of raster into the output file with nodata values.

       Function copied from gdal_merge.py
    """
    try:
        import numpy as Numeric
    except ImportError:
        import Numeric

    s_band = s_fh.GetRasterBand(s_band_n)
    t_band = t_fh.GetRasterBand(t_band_n)

    data_src = s_band.ReadAsArray(s_xoff, s_yoff, s_xsize, s_ysize,
                                  t_xsize, t_ysize)
    data_dst = t_band.ReadAsArray(t_xoff, t_yoff, t_xsize, t_ysize)

    nodata_test = Numeric.equal(data_src, nodata)
    to_write = Numeric.choose(nodata_test, (data_src, data_dst))

    t_band.WriteArray(to_write, t_xoff, t_yoff)

    s_fh = None
    return 0


def check_dir(path):
    """Check if a directory is writable by the user"""
    if not os.access(path, os.W_OK):
        GPDFMessageHandler.error("The directory {dire} is not "
                                 "writable".format(dire=path))
        return False
    return True


def fix_position(val, scr_max, dst_max):
    """Return the new value according a new max value"""
    return val * dst_max / scr_max


def get_image_size(fil):
    """Return the size of a image

    :param str fill: the path to image
    """
    im = Image.open(fil)
    size = im.size
    try:
        im.close()
    except:
        im = None
    return size

def check_bbox(old, new):
    """Check for the maximum bbox of all layers

    :param obj old: list with all
    :param obj new: QgsRectangle object
    """
    if new.xMinimum() < old[0]:
        old[0] = new.xMinimum()
    if new.yMinimum() < old[1]:
        old[1] = new.yMinimum()
    if new.xMaximum() > old[2]:
        old[2] = new.xMaximum()
    if new.yMaximum() > old[3]:
        old[3] = new.yMaximum()
    return old
