# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 08:44:19 2015

@author: lucadelu
"""
IMP_ERROR = 'Python GDAL library not found, please install python-gdal'
try:
    import osgeo.ogr as ogr
except ImportError:
    try:
        import ogr
    except ImportError:
        raise ImportError(IMP_ERROR)
try:
    import osgeo.osr as osr
except ImportError:
    try:
        import osr
    except ImportError:
        raise ImportError(IMP_ERROR)
from gpdf_utils import GPDFMessageHandler
from gpdf_utils import check_bbox
from gpdf_styles import GPDFClassStyle
from types import StringType, ListType, UnicodeType
from qgis.core import QgsDataSourceURI
from qgis.core import QgsFeatureRequest
from qgis.core import QgsRectangle

def srs_from_wkt(wkt):
    """Create osr object from WKT string"""
    srs = osr.SpatialReference()
    srs.ImportFromWkt(wkt)
    return srs


def createMaskFromBbox(coord, qgisrec=False):
    """Function to create an OGR polygon geometry

    :param list coord: a list or a string (space separator) with for values
                  xmin,ymin,xmax,ymax
    :param bool qgisrec: if True return a QGIS rectangle object
    """
    if type(coord) in [StringType, UnicodeType]:
        coord = coord.split(',')
    elif type(coord) != ListType:
        GPDFMessageHandler.error('coord parameter must be a string or a list')
    if len(coord) != 4:
        GPDFMessageHandler.error('coord parameter must contain 4 values: '
                                 'xmin ymin xmax ymax')
    coords = []
    for c in coord:
        coords.append(float(c))
    if qgisrec:
        poly = QgsRectangle(coords[0], coords[1], coords[2], coords[3])
    else:
        poly = ogr.Geometry(ogr.wkbPolygon)
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(coords[0], coords[1])
        ring.AddPoint(coords[0], coords[3])
        ring.AddPoint(coords[2], coords[3])
        ring.AddPoint(coords[2], coords[1])
        ring.AddPoint(coords[0], coords[1])
        poly.AddGeometry(ring)
    return poly


class GPDFVectorLayer:
    """Single vector file"""
    def __init__(self, layer):
        self.qgis = layer
        self.name = self.qgis.name()
        self.source = self.qgis.source()
        self.provider = self.qgis.dataProvider().name()
        self.qgis_style = GPDFClassStyle(self.qgis)
        self.qgis_crs = self.qgis.crs()
        self.symbol = None

    def initialize(self):
        if self.provider == 'postgres':
            pgsource = QgsDataSourceURI(self.source)
            self.source = "PG: dbname='{dname}' host='{host}'" \
                          " port='{port}' user='{us}' " \
                          "password='{pwd}'".format(dname=pgsource.database(),
                                                    host=pgsource.host(),
                                                    port=pgsource.port(),
                                                    us=pgsource.username(),
                                                    pwd=pgsource.password())

        self.input = ogr.Open(self.source)
        if self.input is None:
            GPDFMessageHandler.error('Could not open vector data: '
                                     '{n}'.format(n=self.name))
            return False
        if self.provider in ['postgres']:
            self.layer = self.input.GetLayerByName(str(pgsource.table()))
        else:
            self.layer = self.input.GetLayer(0)
        self.layername = self.layer.GetName()
        self.vtype = self.layer.GetGeomType()
        self.proj = self.layer.GetSpatialRef()
        self.feat_defn = self.layer.GetLayerDefn()
        return True

    def _check_range(self, val):
        """Return the OGR Style string for a value in the range used by
           graduatedSymbol
        """
        for ke, va in self.symbol.iteritems():
            rang = ke.split('_')
            if float(val) >= float(rang[0]) and float(val) <= float(rang[1]):
                return va
            else:
                continue

    def _cut_geometry(self, geom, bbox):
        """Internal function to cut geom with bbox

        :param obj geom: the feature's geometry
        :param obj geom: a ogr geometry to cut input geometry
        """
        if bbox:
            GeomDiff = geom.Intersection(bbox)
            if GeomDiff:
                outGeom = GeomDiff
            else:
                outGeom = geom
        else:
            outGeom = geom
        return outGeom

    def get_extent(self, cut=None):
        """Return the extent in list format"""
        if cut:
            request = QgsFeatureRequest()
            ring = createMaskFromBbox(cut, True)
            request.setFilterRect(ring)
            i = 0
            ogrrect = createMaskFromBbox(cut)

            for feat in self.qgis.getFeatures(request):
                in_feat = self.layer.GetFeature(feat.id())
                if not in_feat:
                    continue
                inGeom = in_feat.GetGeometryRef()
                geom = self._cut_geometry(inGeom, ogrrect)
                if i == 0:
                    rect = geom.GetEnvelope()
                    bbox = [rect[0], rect[2], rect[1], rect[3]]
                else:
                    rect = geom.GetEnvelope()
                    rect = QgsRectangle(rect[0], rect[2], rect[1],
                                        rect[3])
                    bbox = check_bbox(bbox, rect)
                i += 1
            return bbox
        else:
            bbox = self.qgis.extent()
            return [bbox.xMinimum(), bbox.yMinimum(), bbox.xMaximum(),
                    bbox.yMaximum()]

    def write_layer(self, outLayer, cut=None, epsg=None):
        """Write features in a output layer"""
        if self.qgis_style.type_rend == 'singleSymbol':
            self.symbol = self.qgis_style.single_symbol()
        elif self.qgis_style.type_rend == 'categorizedSymbol':
            self.symbol = self.qgis_style.categorized_symbol()
        elif self.qgis_style.type_rend == 'graduatedSymbol':
            self.symbol = self.qgis_style.graduated_symbol()
        self.write_layer_single(outLayer, cut, epsg)
        return True

    def write_layer_single(self, outLayer, cut=None, epsg=None):
        """Write features in a output layer when singleSymbol is used"""
        style_field = ogr.FieldDefn('OGR_STYLE', ogr.OFTString)
        outLayer.CreateField(style_field)
        feat_defn = outLayer.GetLayerDefn()
        if cut:
            ring = createMaskFromBbox(cut)

        for feat in self.qgis.getFeatures():
            in_feat = self.layer.GetFeature(feat.id())
            if not in_feat:
                GPDFMessageHandler.warning("Feature with id {idd} "
                                           "skipped".format(idd=feat.id()))
                continue
            # create a new feature
            out_feat = ogr.Feature(feat_defn)
            # copy the attributes and set geometry
            out_feat.SetFrom(in_feat)
            # get Geometry of feature and apply the difference method
            inGeom = in_feat.GetGeometryRef()
            if cut:
                outGeom = self._cut_geometry(inGeom, ring)
            else:
                outGeom = inGeom

            if not self.proj:
                self.proj = outGeom.GetSpatialReference()
            if epsg and not epsg.IsSame(self.proj):
                outGeom.TransformTo(epsg)

            out_feat.SetGeometry(outGeom)
            if self.qgis_style.type_rend == 'singleSymbol':
                sym = self.symbol
            elif self.qgis_style.type_rend == 'categorizedSymbol':
                sym = self.symbol[feat.attribute(self.qgis_style.class_attr)]
            elif self.qgis_style.type_rend == 'graduatedSymbol':
                sym = self._check_range(feat.attribute(self.qgis_style.class_attr))

            out_feat.SetField('OGR_STYLE', sym)
            out_feat.SetStyleString(sym)
            # add the feature to the output layer
            outLayer.CreateFeature(out_feat)
        return True

    def write_vrt(self, output, epsg=None):
        """Write VRT file to support OGR feature style also with raster data"""
        if self.qgis_style.type_rend == 'singleSymbol':
            self.symbol = self.qgis_style.single_symbol()
            self.write_vrt_layer(output, epsg)
        elif self.qgis_style.type_rend == 'categorizedSymbol':
            self.symbol = self.qgis_style.categorized_symbol()
            for ke, va in self.symbol.iteritems():
                if self.qgis_style.class_type() == StringType:
                    sql = "{key}='{val}'".format(key=self.qgis_style.class_attr,
                                                 val=ke)
                else:
                    sql = "{key}={val}".format(key=self.qgis_style.class_attr,
                                               val=ke)
                self.write_vrt_layer(output, epsg, sql, va)
        elif self.qgis_style.type_rend == 'graduatedSymbol':
            self.symbol = self.qgis_style.graduated_symbol()
            for ke, va in self.symbol.iteritems():
                rang = ke.split('_')
                sql = "{key} BETWEEN {mi} and {ma}".format(mi=rang[0],
                                                           ma=rang[1],
                                                           key=self.qgis_style.class_attr)
                self.write_vrt_layer(output, epsg, sql, va)

    def write_vrt_layer(self, output, epsg=None, where=None, symbol=None):
        """Write single layer to a VRT file to support OGR feature style also
        with raster data
        """
        if where:
            if not symbol:
                kc = where.split('=')
                val = self.qgis_style.class_type(kc[1])
                symbol = self.symbol[val]
            else:
                val = where.split(' ')
                val = '_'.join([val[2], val[4]])
            sel = "\t\t<SrcSQL>SELECT *, '{style}' as OGR_STYLE from {layer}" \
                  " where {query}</SrcSQL>\n".format(layer=self.layername,
                                                     style=symbol,
                                                     query=where)
            lname = "{name}_{cls}".format(name=self.layername, cls=val)
            output.write('\t<OGRVRTLayer name="{name}">\n'.format(name=lname))
        else:
            output.write('\t<OGRVRTLayer name="{name}">\n'.format(name=self.layername))
            sel = "\t\t<SrcSQL>SELECT *, '{style}' as OGR_STYLE from {layer}" \
                  "</SrcSQL>\n".format(layer=self.layername, style=self.symbol)
        output.write('\t\t<SrcDataSource>{name}</SrcDataSource>\n'.format(name=self.source))
        if epsg:
            output.write('\t\t<LayerSRS>{name}</LayerSRS>\n'.format(name=epsg.ExportToWkt()))

        output.write(sel)
        output.write('\t</OGRVRTLayer>\n')


class GPDFVectorClass:
    """Class to work with vector layers"""
    def __init__(self, layers=None):
        self.layers = []
        self.output_vrt = None
        if layers:
            for layer in layers:
                self.add_layer(layer)

    def add_layer(self, layer):
        """Add new layer into layers list"""
        self.layers.append(GPDFVectorLayer(layer))

    def get_extent(self, cut=None):
        bbox = None
        for layer in self.layers:
            if not layer.initialize():
                return False
            if not bbox:
                bbox = layer.get_extent(cut=cut)
            else:
                bbox = check_bbox(bbox, layer.get_extent())
        return bbox

    def write_output(self, output, progress, cut=None, epsg=None):
        """Write output layers for each vector layer visible in QGIS map canvas
        """
        proj = None
        if epsg:
            proj = srs_from_wkt(epsg)
        n = 1
        for layer in self.layers:
            if not layer.initialize():
                return False
            if not proj:
                proj = layer.proj
            outlay = output.CreateLayer(str(layer.name), geom_type=layer.vtype,
                                        srs=proj)
            for n in range(layer.feat_defn.GetFieldCount()):
                outlay.CreateField(layer.feat_defn.GetFieldDefn(n))
            layer.write_layer(outlay, cut, proj)
            outlay = None
            n += 1
            progress.setValue(n)
        return True

    def write_vrt(self, epsg=None):
        """Write a VRT file"""
        import tempfile
        if epsg:
            epsg = srs_from_wkt(epsg)
        output = tempfile.NamedTemporaryFile(delete=False)
        self.output_vrt = output.name
        output.write('<OGRVRTDataSource>\n')
        for layer in self.layers:
            layer.initialize()
            layer.write_vrt(output, epsg)
        output.write('</OGRVRTDataSource>\n')
        output.close()


class GPDFRasterLayer:
    """Single raster file"""
    def __init__(self, layer):
        self.qgis = layer
        self.name = self.qgis.name()
        self.source = self.qgis.source()

    def get_extent(self):
        """Return the extent in list format"""
        rect = self.qgis.extent()
        return [rect.xMinimum(), rect.yMinimum(), rect.xMaximum(),
                rect.yMaximum()]


class GPDFRasterClass:
    """Class to work with raster layers"""
    def __init__(self, layers=None):
        self.layers = []
        if layers:
            for layer in layers:
                self.add_layer(layer)

    def add_layer(self, layer):
        """Add new layer into layers list"""
        self.layers.append(GPDFRasterLayer(layer))
