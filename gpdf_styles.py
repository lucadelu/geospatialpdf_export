# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 10:31:23 2015

@author: lucadelu
"""
from gpdf_utils import GPDFMessageHandler

PEN = "PEN(c:{col}"
SYMBOL = 'SYMBOL(id:"{sim}"'
BRUSH = 'BRUSH('
FCOLOR = 'fc:{col}'
BCOLOR = 'bc:{col}'
COLOR = 'c:{col}'
OUTCOLOR = 'o:{col}'
ID = 'id:"{sym}"'
DIM = "w:{dim}{unit}"


class GPDFClassStyle:
    """Class to obtain the style of features"""
    def __init__(self, layer):
        self.layer = layer
        self.name = self.layer.name()
        self.source = layer.source()
        self.type_geom = self.layer.geometryType()
        self.renderer = self.layer.rendererV2()
        self.type_rend = str(self.renderer.type())
        self.class_attr = None
        self.class_type = None

    def _return_ogr_style(self, symbol):
        """Function to return a OGR Feature Style string"""
        self.check_symbol(symbol)
        props = symbol.symbolLayer(0).properties()
        self.check_proprierties(props)
        if self.type_geom == 0:
            return marker(props)
        elif self.type_geom == 1:
            return line(props)
        elif self.type_geom == 2:
            return polygon(props)

    def single_symbol(self):
        """Return OGR feature style string"""
        symbol = self.renderer.symbol()
        return self._return_ogr_style(symbol)

    def categorized_symbol(self):
        """Return a dictionary with classes value and OGR feature style
           string
        """
        self.class_attr = str(self.renderer.classAttribute())
        out = {}
        categories = self.renderer.categories()
        self.class_type = type(categories[0].value())
        for cat in categories:
            symbol = cat.symbol()
            out[cat.value()] = self._return_ogr_style(symbol)
        return out

    def graduated_symbol(self):
        """Return a dictionary with class value and OGR feature style string"""
        self.class_attr = str(self.renderer.classAttribute())
        out = {}
        symbols = self.renderer.symbols()
        ranges = self.renderer.ranges()
        if len(symbols) != len(ranges):
            GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                       "Layer {n} has problem with number of "
                                       "styles and ranges".format(n=self.name))
        else:
            for i in range(len(symbols)):
                rang = "{mi}_{ma}".format(mi=ranges[i].lowerValue(),
                                          ma=ranges[i].upperValue())
                symbol = symbols[i]
                out[rang] = self._return_ogr_style(symbol)
            return out

    def check_proprierties(self, prop):
        """Check if the style of rendering it is supported by OGR Feature Style
        """
        if self.type_geom == 0:
            if 'color' not in prop.keys() and 'outline_color' not in prop.keys():
                GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                           "Layer {n} has problem with OGR "
                                           "Feature Style".format(n=self.name))
        if self.type_geom == 1:
            if 'line_color' not in prop.keys():
                GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                           "Layer {n} has problem with OGR "
                                           "Feature Style".format(n=self.name))

    def check_symbol(self, symbol):
        """Check if a symbol could have any problem for OGR feature style"""
        if symbol.symbolLayerCount() != 1:
            GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                       "On vector <b>{name}</b>, style "
                                       "type {style}, first symbol is "
                                       "used".format(name=self.source,
                                                     style=self.type_rend))


def check_unit(unit):
    """Check if unit is supported and return the right value"""
    unitlow = unit.lower()
    if unitlow in ['px', 'pt', 'mm', 'cm', 'in']:
        return unitlow
    elif unitlow == "mapunit":
        return 'g'
    else:
        GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                   "Unit not supported. The supported unit "
                                   "values are 'px', 'pt', 'mm', 'cm', 'in'"
                                   "and Map Unit")


def value_no_0(value, code):
    """Check if a value is used in QGIS or not"""
    if value != '0':
        return '{co}:{val}'.format(co=code, val=value)
    else:
        return None


def rotation(value, default=45.0):
    """Check if a angle it is set and report the correct one according to the
    symbol. It is used for QGIS marker 'diamond' (45°) and 'triangle2' (90°)
    """
    if value != '0':
        final = default + float(value)
    else:
        final = default
    return 'a:{val}'.format(val=final)


def get_cap(cap):
    """Return the OGR code for the QGIS Cap style"""
    if cap == 'square':
        return 'p'
    elif cap == 'flat':
        return 'b'
    elif cap == 'round':
        return 'r'
    else:
        GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                   "No matching Cap style found. Set the "
                                   "default of OGR")
        return 'b'


def get_join(join):
    """Return the OGR code for the QGIS Join style"""
    if join == 'bevel':
        return 'b'
    elif join == 'miter':
        return 'm'
    elif join == 'round':
        return 'r'
    else:
        GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                   "No matching Cap style found. Set the "
                                   "default of OGR")
        return 'm'


def polygon(symbol, layer=0):
    """Return the OGR Feature Style symbol string for polygon"""
    out = []
    if layer == 0:
        color = rgb_to_hex(str_to_rgba(symbol['color']))
        out.append(BRUSH + FCOLOR.format(col=color))

    if symbol['style'] == 'no':
        out.append(ID.format(sym='ogr-brush-1'))
    elif symbol['style'] == 'horizontal':
        out.append(ID.format(sym='ogr-brush-2'))
    elif symbol['style'] == 'vertical':
        out.append(ID.format(sym='ogr-brush-3'))
    elif symbol['style'] == 'f_diagonal':
        out.append(ID.format(sym='ogr-brush-4'))
    elif symbol['style'] == 'b_diagonal':
        out.append(ID.format(sym='ogr-brush-5'))
    elif symbol['style'] == 'cross':
        out.append(ID.format(sym='ogr-brush-6'))
    elif symbol['style'] == 'diagonal_x':
        out.append(ID.format(sym='ogr-brush-7'))
    # check the outline style
    if symbol['outline_style'] != 'no':
        lin = line(symbol, 'outline')
    final = ','.join(out)
    final += ')'
    return ';'.join((final, lin))


def line(symbol, pref='line'):
    """Return the OGR Feature Style symbol string for line"""
    out = []
    key = '{p}_color'.format(p=pref)
    out.append(PEN.format(col=rgb_to_hex(str_to_rgba(symbol[key]))))
    key = '{p}_width'.format(p=pref)
    key1 = '{p}_width_unit'.format(p=pref)
    out.append(DIM.format(dim=symbol[key], unit=check_unit(symbol[key1])))
    out.append('j:{join}'.format(join=get_join(symbol['joinstyle'])))
    try:
        out.append('cap:{cap}'.format(cap=get_cap(symbol['capstyle'])))
    except:
        pass
    key = '{p}_style'.format(p=pref)
    if symbol[key] == 'solid':
        pass
    elif symbol[key] == 'dash':
        out.append(ID.format(sym='ogr-pen-2'))
    elif symbol[key] == 'dot':
        out.append(ID.format(sym='ogr-pen-5'))
    elif symbol[key] == 'dash dot':
        out.append(ID.format(sym='ogr-pen-6'))
    elif symbol[key] == 'dash dot dot':
        out.append(ID.format(sym='ogr-pen-7'))
    final = ','.join(out)
    return final + ')'


def marker(symbol):
    """Return the OGR Feature Style symbol string for points"""

    def check_star(star):
        """Check the QGIS star type
        """
        # TODO
    out = []
    # cross
    if symbol['name'] == "cross":
        out.append(SYMBOL.format(sim="ogr-sym-0"))
        color = rgb_to_hex(str_to_rgba(symbol['outline_color']))
        out.append(COLOR.format(col=color))
    # diagonal cross
    elif symbol['name'] == "cross2":
        out.append(SYMBOL.format(sim="ogr-sym-1"))
        color = rgb_to_hex(str_to_rgba(symbol['outline_color']))
        out.append(COLOR.format(col=color))
    # circle (not filled)
    elif symbol['name'] in ["circle", "city"] and symbol['color'] == '0,0,0,255':
        out.append(SYMBOL.format(sim="ogr-sym-2"))
    # circle (filled)
    elif symbol['name'] in ["circle", "city"]:
        out.append(SYMBOL.format(sim="ogr-sym-3"))
        color = rgb_to_hex(str_to_rgba(symbol['color']))
        out.append(COLOR.format(col=color))
    # square (not filled)
    elif symbol['name'] in ['square', 'diamond'] and symbol['color'] == '0,0,0,255':
        out.append(SYMBOL.format(sim="ogr-sym-4"))
        if symbol['name'] == 'diamond':
            out.append(rotation(symbol['angle']))
    # square (filled)
    elif symbol['name'] in ['square', 'diamond']:
        out.append(SYMBOL.format(sim="ogr-sym-5"))
        color = rgb_to_hex(str_to_rgba(symbol['color']))
        out.append(COLOR.format(col=color))
        if symbol['name'] == 'diamond':
            out.append(rotation(symbol['angle']))
    # triangle (not filled)
    elif symbol['name'] in ['triangle', 'triangle2'] and symbol['color'] == '0,0,0,255':
        out.append(SYMBOL.format(sim="ogr-sym-6"))
        if symbol['name'] == 'triangle2':
            out.append(rotation(symbol['angle'], 90.))
        return "ogr-sym-6"
    # triangle (filled)
    elif symbol['name'] in ['triangle', 'triangle2']:
        out.append(SYMBOL.format(sim="ogr-sym-7"))
        color = rgb_to_hex(str_to_rgba(symbol['color']))
        out.append(COLOR.format(col=color))
        if symbol['name'] == 'triangle2':
            out.append(rotation(symbol['angle'], 90.))
    # star (not filled)
    elif symbol['name'] in ['start', 'start2', 'start3'] and symbol['color'] == '0,0,0,255':
        out.append(SYMBOL.format(sim="ogr-sym-8"))
    # star (filled)
    elif symbol['name'] in ['start', 'start2', 'start3']:
        out.append(SYMBOL.format(sim="ogr-sym-9"))
        color = rgb_to_hex(str_to_rgba(symbol['color']))
        out.append(COLOR.format(col=color))
    # vertical bar (can be rotated using angle attribute to produce diag bar)
    # elif symbol['name'] == "":
        # return "ogr-sym-10"
    else:
        GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                   "No matching symbol found. Set by default"
                                   " circle filled")
        # circle (filled)
        out.append(SYMBOL.format(sim="ogr-sym-2"))
        color = rgb_to_hex(str_to_rgba(symbol['color']))
        out.append(COLOR.format(col=color))
    if symbol['outline_style'] != 'no':
        color = rgb_to_hex(str_to_rgba(symbol['outline_color']))
        out.append(OUTCOLOR.format(col=color))
    # add size
    dim = DIM.format(dim=symbol['size'], unit=check_unit(symbol['size_unit']))
    out.append(dim)
    if symbol['name'] != 'diamond':
        # check angle
        value = value_no_0(symbol['angle'], 'a')
        if value:
            out.append(value)
    # check X and Y offset
    final = ','.join(out)
    return final + ')'


def rgb_to_hex(rgb):
    """Return hex string from list of RGB values"""
    if len(rgb) == 3:
        return '#%02x%02x%02x' % rgb
    elif len(rgb) == 4:
        return '#%02x%02x%02x%02x' % rgb
    else:
        GPDFMessageHandler.warning("GeospatialPDF: Problem with style",
                                   "Only RGB and RGBA format are supported."
                                   "Please pass a list with three or four "
                                   "values")


def str_to_rgba(string):
    """Convert a string with RGBA color to a list"""
    out = []
    for s in string.split(','):
        out.append(int(s))
    return tuple(out)
