GeospatialPDF export Plugin (c)  for QGIS
(c) Copyright Luca Delucchi - 2015
Authors: Luca Delucchi
Email: luca.delucchi fmach it

GeospatialPDF export Plugin is licensed under the terms of GNU GPL 2
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

More info https://gitlab.com/lucadelu/geospatialpdf_export
