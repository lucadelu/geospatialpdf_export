"""
Created on Tue Jan 27 09:57:54 2015

@author: lucadelu
"""
#pylint: disable=no-name-in-module
from PyQt4.QtCore import SIGNAL, QObject, QDateTime
from PyQt4.QtGui import QDialog, QAction, QIcon, QFileDialog, QMessageBox
from PyQt4.QtGui import QTableWidgetItem
#pylint: enable=no-name-in-module
import os
from gpdf_ui import Ui_GeospatialPDF
from qgis.core import QgsMapLayerRegistry
import osgeo.ogr as ogr
import osgeo.gdal as gdal
from gpdf_utils import GPDFMessageHandler
from gpdf_utils import check_dir
from gpdf_utils import fix_position
from gpdf_utils import get_image_size
from gpdf_utils import check_bbox
from gpdf_layers import GPDFVectorClass
from gpdf_layers import GPDFRasterClass
from qgis.gui import QgsProjectionSelectionWidget

FORMAT = 'PDF'
MSG_BOX_TITLE = "GeospatialPDF export"
DEFAULT_DPI = 72
USER_UNIT_IN_INCH = 1.0 / DEFAULT_DPI
MAXIMUM_SIZE_IN_UNITS = 14400
gdal.UseExceptions()


class UiGeospatialPDFDialog(QDialog):
    """The dialog for the plugin"""
    def __init__(self):
        QDialog.__init__(self)
        # Set up the user interface from Designer.
        self.gui = Ui_GeospatialPDF()
        self.gui.setupUi(self)


class GeospatialPDF:
    """The actions for the plugin"""
    def __init__(self, iface):
        self.iface = iface
        self.mapcanvas = self.iface.mapCanvas()
        self.registry = QgsMapLayerRegistry.instance()
        self.vectors = []
        self.rasters = []
        self.dlg = None
        self.action = None
        self.output_name = None
        self.cut = None
        self.opts = []
        self.total_layers = None
        self.bbox = None

    def initGui(self):
        """Method to add the plugin to QGIS"""
        self.action = QAction(QIcon(os.path.join(os.path.dirname(__file__),
                                                 "images", "logo.png")),
                              "GeospatialPDF export", self.iface.mainWindow())
        self.action.setWhatsThis("GeospatialPDF export plugin")
        QObject.connect(self.action, SIGNAL("triggered()"), self.run)
        # add plugin to the web plugin menu
        self.iface.addPluginToMenu("&GeospatialPDF export", self.action)
        # and add button to the Web panel
        self.iface.addToolBarIcon(self.action)

    def unload(self):
        """Method to unload the plugin to QGIS"""
        # new menu used, remove submenus from main Web menu
        self.iface.removePluginMenu("&GeospatialPDF export", self.action)
        # also remove button from Web toolbar
        self.iface.removeToolBarIcon(self.action)

    def run(self):
        """Method before load the plugin"""
        self.opts = []
        self.dlg = UiGeospatialPDFDialog()
        if self.mapcanvas.layerCount() == 0:
            GPDFMessageHandler.error("No active layer found. Please make "
                                     "one or more layer active")
            return
        self.vectors = GPDFVectorClass()
        self.rasters = GPDFRasterClass()
        for i in range(self.mapcanvas.layerCount()-1, -1, -1):
            layer = self.mapcanvas.layer(i)
            dataprov = layer.dataProvider().name()
            # check if is a vector
            if layer.type() == layer.VectorLayer:
                self.vectors.add_layer(layer)
                source = '{name} ({type})'.format(name=layer.name(),
                                                  type=dataprov)
                self.dlg.gui.LayerList.addItem(source)
            if layer.type() == layer.RasterLayer:
                self.rasters.add_layer(layer)
                source = '{name} ({type})'.format(name=layer.name(),
                                                  type=dataprov)
                self.dlg.gui.RasterList.addItem(source)

        self.total_layers = self.mapcanvas.layerCount() + 1
        self.dlg.gui.progressBar.setMinimum(0)
        self.dlg.gui.progressBar.setMaximum(self.total_layers)

        self.dlg.gui.creator.setText("GeospatialPDF export for QGIS")
        self.dlg.gui.producer.setText("GDAL version {ver}".format(ver=gdal.__version__))

        self.coords = QgsProjectionSelectionWidget()
        self.dlg.gui.horizontalLayout_24.addWidget(self.coords)
        # button for start the plugin
        QObject.connect(self.dlg.gui.buttonBox, SIGNAL("accepted()"),
                        self.convert)
        # button for close the plugin after create openlayers file
        QObject.connect(self.dlg.gui.buttonBox, SIGNAL("rejected()"),
                        self.dlg.close)
        # select directory where save files
        QObject.connect(self.dlg.gui.browseButton, SIGNAL("clicked()"),
                        self.select_out_dir)
        # add function on tab changing
        QObject.connect(self.dlg.gui.addimages, SIGNAL("clicked()"),
                        self.get_file)
        # set or unset bounding box from QGIS map canvas
        QObject.connect(self.dlg.gui.bbox, SIGNAL("clicked()"),
                        self.set_bbox)
        # if user start to compile the author information the checkbox it will
        # be activate
        QObject.connect(self.dlg.gui.author, SIGNAL("editingFinished()"),
                        self.author_activate)
        QObject.connect(self.dlg.gui.creator, SIGNAL("editingFinished()"),
                        self.author_activate)
        QObject.connect(self.dlg.gui.producer, SIGNAL("editingFinished()"),
                        self.author_activate)
        QObject.connect(self.dlg.gui.subject, SIGNAL("editingFinished()"),
                        self.author_activate)
        QObject.connect(self.dlg.gui.title, SIGNAL("editingFinished()"),
                        self.author_activate)
        QObject.connect(self.dlg.gui.keywords, SIGNAL("editingFinished()"),
                        self.author_activate)
        # set action on checkbox of layout
        QObject.connect(self.dlg.gui.topleft, SIGNAL("clicked()"),
                        lambda: self.set_pos('Top-left'))
        QObject.connect(self.dlg.gui.topright, SIGNAL("clicked()"),
                        lambda: self.set_pos('Top-right'))
        QObject.connect(self.dlg.gui.middleleft, SIGNAL("clicked()"),
                        lambda: self.set_pos('Middle-left'))
        QObject.connect(self.dlg.gui.middleright, SIGNAL("clicked()"),
                        lambda: self.set_pos('Middle-right'))
        QObject.connect(self.dlg.gui.bottomleft, SIGNAL("clicked()"),
                        lambda: self.set_pos('Bottom-left'))
        QObject.connect(self.dlg.gui.bottomright, SIGNAL("clicked()"),
                        lambda: self.set_pos('Bottom-right'))
        self.set_bbox()
        self.get_bbox()

        # set today as day for creation
        qd = QDateTime()
        self.dlg.gui.date.setDateTime(qd.currentDateTime())
        # show datetime
        self.dlg.show()

    def get_bbox(self):
        if self.dlg.gui.bbox.isChecked():
            rect = self.mapcanvas.extent()
            self.bbox = [rect.xMinimum(), rect.yMinimum(),
                         rect.xMaximum(), rect.yMaximum()]
        else:
            if len(self.rasters.layers) > 0:
                for layer in self.rasters.layers:
                    self.bbox = check_bbox(self.bbox,
                                           layer.get_extent())
            elif len(self.vectors.layers) > 0:
                for layer in self.vectors.layers:
                    self.bbox = check_bbox(self.bbox,
                                           layer.get_extent())

    def get_file(self):
        """Add an image file to the table widget as element for the
        output PDF"""
        file_name = QFileDialog.getOpenFileName(None, "Choose element",
                                                "",
                                                "Images (*.png *.jpg)",
                                                options=QFileDialog.DontUseNativeDialog)
        if file_name:
            rownum = self.dlg.gui.content_list.rowCount()
            self.dlg.gui.content_list.insertRow(rownum)
            self.dlg.gui.content_list.setItem(rownum, 0,
                                              QTableWidgetItem(file_name))
            self.dlg.gui.content_list.setItem(rownum, 1,
                                              QTableWidgetItem('100'))
            self.dlg.gui.content_list.setItem(rownum, 2,
                                              QTableWidgetItem('Top-left'))

    def select_out_dir(self):
        """Method to select the output file"""
        # set up the output dir for new vector files
        name = QFileDialog.getSaveFileName(parent=None,
                                           caption='Choose PDF output',
                                           directory='',
                                           options=QFileDialog.DontUseNativeDialog)
        if name:
            try:
                if not name.endswith('.pdf'):
                    name += '.pdf'
                self.dlg.gui.outdirpath.setText(name)
            except (IOError, OSError):
                GPDFMessageHandler.error("It is not possible to write into "
                                         "folder '{o}'.".format(o=name))
                return False

    def set_pos(self, text):
        """Set the position of extra image into table widget"""
        index = self.dlg.gui.content_list.selectionModel().selectedRows()
        if len(index) != 1:
            GPDFMessageHandler.warning("Select one row in the element table")
            return 1
        self.dlg.gui.content_list.setItem(index[0].row(), 2,
                                          QTableWidgetItem(text))

    def author_activate(self):
        """Set to true the checkbox of metadata"""
        if not self.dlg.gui.metadata.isChecked():
            self.dlg.gui.metadata.setChecked(True)

    def set_bbox(self):
        """Set the QGIS mapcanvas extent into LineEdit"""
        if self.dlg.gui.bbox.isChecked():
            rect = self.mapcanvas.extent()
            text = ','.join([str(rect.xMinimum()), str(rect.yMinimum()),
                             str(rect.xMaximum()), str(rect.yMaximum())])
            self.dlg.gui.extent.setText(text)
        else:
            self.dlg.gui.extent.clear()

    def get_pos(self, pos, fil, dpi):
        """Return the position from left bottom coordinate

        :param str pos: the text of position
        :param str fil: the path of file
        :param int dpi: the dpi value to use
        """
        # TODO improve the shift in latlong
        diffw = self.bbox[2] - self.bbox[0]
        diffh = self.bbox[3] - self.bbox[1]
        shift = 5
        if len(self.rasters.layers) == 0:
            dfRatio = diffh / diffw
            if dfRatio < 1:
                pdfwidth = 1024
                pdfheight = int(pdfwidth * dfRatio)
            else:
                pdfheight = 1024
                pdfwidth = int(pdfheight / dfRatio)
        else:
            dfUserUnit = dpi * USER_UNIT_IN_INCH;
            first_rast = self.rasters.layers[0]
            in_raster = gdal.Open(first_rast.source)
            if self.cut:
                geotra = in_raster.GetGeoTransform()
                inwidth = diffw / geotra[1]
                inheight = diffh / abs(geotra[5])
            else:
                inwidth = in_raster.RasterXSize
                inheight = in_raster.RasterYSize
            # TODO add margin option
            shift = int(shift / dfUserUnit)
            if shift < 1:
                shift = 1
            pdfwidth = int(inwidth / dfUserUnit) #+ sMargins.nLeft + sMargins.nRight;
            pdfheight = int(inheight / dfUserUnit) #+ sMargins.nBottom + sMargins.nTop;
        if pdfwidth > MAXIMUM_SIZE_IN_UNITS:
            pdfwidth = MAXIMUM_SIZE_IN_UNITS
        if pdfheight > MAXIMUM_SIZE_IN_UNITS:
            pdfheight = MAXIMUM_SIZE_IN_UNITS
        width, height = get_image_size(fil)

        if dpi > 72:
            width = int(width / dfUserUnit)
            height = int(height / dfUserUnit)

        if pos == 'Top-left':
            x = shift
            y =  pdfheight- shift - height
        elif pos == 'Top-right':
            x = pdfwidth - shift - width
            y = pdfheight - shift - height
        elif pos == 'Middle-left':
            x = shift
            y = (pdfheight / 2) - (height / 2)
        elif pos == 'Middle-right':
            x = pdfwidth - shift - width
            y = (pdfheight / 2) - (height / 2)
        elif pos == 'Bottom-left':
            x = shift
            y = shift
        elif pos == 'Bottom-right':
            x = pdfwidth - shift - width
            y = shift

        return x, y

    def get_crs(self):
        """Check if a SRS is set"""
        if not self.coords.crs().isValid():
            return None
        else:
            return self.coords.crs().toWkt()

    def convert(self):
        """Method to run the plugin to QGIS"""
        self.output_name = self.dlg.gui.outdirpath.text()
        dirname = os.path.dirname(self.output_name)
        if self.output_name == '':
            GPDFMessageHandler.error("Please select the output file first")
            return
        elif os.path.exists(self.output_name):
            res = QMessageBox.question(None, "GeospatialPDF Plugin",
                                       u"{0} already exists. Replace "
                                       "it?".format(self.output_name),
                                       QMessageBox.Yes | QMessageBox.No,
                                       QMessageBox.No)

            if res == QMessageBox.Yes:
                os.remove(self.output_name)
                if not check_dir(dirname):
                    self.dlg.close()
                    return
            else:
                return
        else:
            if not check_dir(dirname):
                self.dlg.close()
                return
        self.dlg.gui.progressBar.setValue(1)
        # compbboxression
        self.opts.append('COMPRESS={var}'.format(var=self.dlg.gui.compr.currentText()))
        # dpi
        dpi = int(self.dlg.gui.dpi.text())
        if dpi < 72:
            dpi = 72
        self.opts.append('DPI={var}'.format(var=dpi))
        self.opts.append('GEO_ENCODING=BOTH')
        # extent
        if self.dlg.gui.extent.text():
            self.cut = self.dlg.gui.extent.text()
            self.opts.append('CLIPPING_EXTENT={var}'.format(var=self.cut))
        # tiles
        if self.dlg.gui.tiles.isChecked():
            self.opts.append('TILED=YES')
            self.opts.append('BLOCKXSIZE={var}'.format(var=self.dlg.gui.tilew.text()))
            self.opts.append('BLOCKYSIZE={var}'.format(var=self.dlg.gui.tileh.text()))
        if self.dlg.gui.java.text():
            self.opts.append('JAVASCRIPT_FILE={var}'.format(var=self.dlg.gui.tilew.text()))

        if self.dlg.gui.metadata.isChecked():
            dt = self.dlg.gui.date.dateTime()
            self.opts.append('CREATION_DATE=D:{var}'.format(var=dt.toString('yyyyMMddhhmmss')))
            self.opts.append('WRITE_INFO=YES')
            if self.dlg.gui.author.text():
                self.opts.append('AUTHOR={var}'.format(var=self.dlg.gui.author.text()))
            if self.dlg.gui.creator.text():
                self.opts.append('CREATOR={var}'.format(var=self.dlg.gui.creator.text()))
            if self.dlg.gui.producer.text():
                self.opts.append('PRODUCER={var}'.format(var=self.dlg.gui.producer.text()))
            if self.dlg.gui.subject.text():
                self.opts.append('SUBJECT={var}'.format(var=self.dlg.gui.subject.text()))
            if self.dlg.gui.title_out.text():
                self.opts.append('TITLE={var}'.format(var=self.dlg.gui.title_out.text()))
            if self.dlg.gui.keywords.text():
                self.opts.append('KEYWORDS={var}'.format(var=self.dlg.gui.keywords.text()))
        else:
            self.opts.append('WRITE_INFO=NO')

        rows = self.dlg.gui.content_list.rowCount()
        if rows > 0:
            extra = "EXTRA_IMAGES="
            if len(self.rasters.layers) == 0:
                self.bbox = self.vectors.get_extent(self.cut)
            for n in range(0, rows):
                if n > 0:
                    extra += ","

                path = self.dlg.gui.content_list.item(n, 0)
                if path:
                    path = path.text()
                scale = int(self.dlg.gui.content_list.item(n, 1).text()) / 100.
                pos = self.dlg.gui.content_list.item(n, 2).text()
                link = self.dlg.gui.content_list.item(n, 3)
                x, y = self.get_pos(pos, path, dpi)
                extra += "{fi},{x},{y},{sc}".format(fi=path, x=x, y=y,
                                                    sc=scale)
                if link:
                    extra += ",{li}".format(li=link.text())
            self.opts.append(extra)

        if len(self.rasters.layers) == 0:
            out = self.convert_only_vector()
        else:
            out = self.convert_all()
        if out:
            GPDFMessageHandler.success("GeospatialPDF '{na}' created "
                                       "correctly".format(na=self.output_name))
        self.dlg.close()

    def convert_only_vector(self):
        """Method to convert only vector file"""
        driver = ogr.GetDriverByName(FORMAT)
        try:
            outfile = driver.CreateDataSource(self.output_name,
                                              options=self.opts)
        except Exception, e:
            GPDFMessageHandler.error("Output file creation failed: {er}".format(er=e))
            return False
        srs = self.get_crs()
        if not self.vectors.write_output(outfile, self.dlg.gui.progressBar,
                                         self.cut, srs):
            return False
        outfile = None
        return True

    def convert_all(self):
        """Method to convert raster and/or vector"""
        rast_driver = gdal.GetDriverByName(FORMAT)

        self.vectors.write_vrt()
        first_rast = self.rasters.layers[0]
        in_raster = gdal.Open(first_rast.source)
        self.opts.append('OGR_DATASOURCE={path}'.format(path=self.vectors.output_vrt))
        if len(self.rasters.layers) > 1:
            names = []
            sources = []
            for n in range(len(self.rasters.layers)):
                raster = self.rasters.layers[n]
                if n == 0:
                    self.opts.append('LAYER_NAME={va}'.format(va=raster.name))
                else:
                    sources.append(raster.source)
                    names.append(raster.name)
            self.opts.append('EXTRA_RASTERS={path}'.format(path=','.join(sources)))
            self.opts.append('EXTRA_RASTERS_LAYER_NAME={pa}'.format(pa=','.join(names)))
        try:
            outfile = rast_driver.CreateCopy(self.output_name, in_raster,
                                             options=self.opts)
        except RuntimeError, e:
            GPDFMessageHandler.error(str(e))
            return False
        in_raster = None
        outfile = None
        os.remove(self.vectors.output_vrt)
        self.dlg.gui.progressBar.setValue(self.total_layers)
        return True
