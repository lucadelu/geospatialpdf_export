# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gpdf.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_GeospatialPDF(object):
    def setupUi(self, GeospatialPDF):
        GeospatialPDF.setObjectName(_fromUtf8("GeospatialPDF"))
        GeospatialPDF.resize(852, 476)
        self.verticalLayout_3 = QtGui.QVBoxLayout(GeospatialPDF)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.icon = QtGui.QLabel(GeospatialPDF)
        self.icon.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.icon.setText(_fromUtf8(""))
        self.icon.setPixmap(QtGui.QPixmap(_fromUtf8(":/geospatialpdf/images/logo.png")))
        self.icon.setAlignment(QtCore.Qt.AlignCenter)
        self.icon.setObjectName(_fromUtf8("icon"))
        self.horizontalLayout.addWidget(self.icon)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.title = QtGui.QLabel(GeospatialPDF)
        font = QtGui.QFont()
        font.setFamily(_fromUtf8("Trebuchet MS"))
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.title.setFont(font)
        self.title.setFrameShape(QtGui.QFrame.NoFrame)
        self.title.setObjectName(_fromUtf8("title"))
        self.verticalLayout.addWidget(self.title)
        self.subtitle = QtGui.QLabel(GeospatialPDF)
        self.subtitle.setFrameShape(QtGui.QFrame.NoFrame)
        self.subtitle.setObjectName(_fromUtf8("subtitle"))
        self.verticalLayout.addWidget(self.subtitle)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.tabWidget = QtGui.QTabWidget(GeospatialPDF)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setEnabled(True)
        self.tab.setObjectName(_fromUtf8("tab"))
        self.verticalLayout_13 = QtGui.QVBoxLayout(self.tab)
        self.verticalLayout_13.setObjectName(_fromUtf8("verticalLayout_13"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout_10 = QtGui.QHBoxLayout()
        self.horizontalLayout_10.setObjectName(_fromUtf8("horizontalLayout_10"))
        self.label_ogr = QtGui.QLabel(self.tab)
        self.label_ogr.setObjectName(_fromUtf8("label_ogr"))
        self.horizontalLayout_10.addWidget(self.label_ogr)
        self.gridLayout.addLayout(self.horizontalLayout_10, 0, 0, 1, 1)
        self.LayerList = QtGui.QListWidget(self.tab)
        self.LayerList.setObjectName(_fromUtf8("LayerList"))
        self.gridLayout.addWidget(self.LayerList, 1, 0, 1, 1)
        self.horizontalLayout_11 = QtGui.QHBoxLayout()
        self.horizontalLayout_11.setObjectName(_fromUtf8("horizontalLayout_11"))
        self.label_gdal = QtGui.QLabel(self.tab)
        self.label_gdal.setEnabled(True)
        self.label_gdal.setObjectName(_fromUtf8("label_gdal"))
        self.horizontalLayout_11.addWidget(self.label_gdal)
        self.gridLayout.addLayout(self.horizontalLayout_11, 2, 0, 1, 1)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_out = QtGui.QLabel(self.tab)
        self.label_out.setObjectName(_fromUtf8("label_out"))
        self.horizontalLayout_4.addWidget(self.label_out)
        self.gridLayout.addLayout(self.horizontalLayout_4, 4, 0, 1, 1)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.outdirpath = QtGui.QLineEdit(self.tab)
        self.outdirpath.setObjectName(_fromUtf8("outdirpath"))
        self.horizontalLayout_3.addWidget(self.outdirpath)
        self.browseButton = QtGui.QPushButton(self.tab)
        self.browseButton.setObjectName(_fromUtf8("browseButton"))
        self.horizontalLayout_3.addWidget(self.browseButton)
        self.gridLayout.addLayout(self.horizontalLayout_3, 5, 0, 1, 1)
        self.RasterList = QtGui.QListWidget(self.tab)
        self.RasterList.setEnabled(True)
        self.RasterList.setObjectName(_fromUtf8("RasterList"))
        self.gridLayout.addWidget(self.RasterList, 3, 0, 1, 1)
        self.verticalLayout_13.addLayout(self.gridLayout)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(_fromUtf8("tab_3"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.tab_3)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_24 = QtGui.QHBoxLayout()
        self.horizontalLayout_24.setObjectName(_fromUtf8("horizontalLayout_24"))
        self.label_3 = QtGui.QLabel(self.tab_3)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_24.addWidget(self.label_3)
        self.verticalLayout_2.addLayout(self.horizontalLayout_24)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_compr = QtGui.QLabel(self.tab_3)
        self.label_compr.setObjectName(_fromUtf8("label_compr"))
        self.horizontalLayout_6.addWidget(self.label_compr)
        self.compr = QtGui.QComboBox(self.tab_3)
        self.compr.setObjectName(_fromUtf8("compr"))
        self.compr.addItem(_fromUtf8(""))
        self.compr.addItem(_fromUtf8(""))
        self.compr.addItem(_fromUtf8(""))
        self.compr.addItem(_fromUtf8(""))
        self.horizontalLayout_6.addWidget(self.compr)
        self.verticalLayout_2.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        self.label_dpi = QtGui.QLabel(self.tab_3)
        self.label_dpi.setObjectName(_fromUtf8("label_dpi"))
        self.horizontalLayout_7.addWidget(self.label_dpi)
        self.dpi = QtGui.QLineEdit(self.tab_3)
        self.dpi.setAlignment(QtCore.Qt.AlignCenter)
        self.dpi.setObjectName(_fromUtf8("dpi"))
        self.horizontalLayout_7.addWidget(self.dpi)
        self.verticalLayout_2.addLayout(self.horizontalLayout_7)
        self.horizontalLayout_8 = QtGui.QHBoxLayout()
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.label_extent = QtGui.QLabel(self.tab_3)
        self.label_extent.setObjectName(_fromUtf8("label_extent"))
        self.horizontalLayout_8.addWidget(self.label_extent)
        self.extent = QtGui.QLineEdit(self.tab_3)
        self.extent.setObjectName(_fromUtf8("extent"))
        self.horizontalLayout_8.addWidget(self.extent)
        self.bbox = QtGui.QCheckBox(self.tab_3)
        self.bbox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.bbox.setChecked(True)
        self.bbox.setObjectName(_fromUtf8("bbox"))
        self.horizontalLayout_8.addWidget(self.bbox)
        self.verticalLayout_2.addLayout(self.horizontalLayout_8)
        self.horizontalLayout_20 = QtGui.QHBoxLayout()
        self.horizontalLayout_20.setObjectName(_fromUtf8("horizontalLayout_20"))
        self.tiles = QtGui.QCheckBox(self.tab_3)
        self.tiles.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.tiles.setObjectName(_fromUtf8("tiles"))
        self.horizontalLayout_20.addWidget(self.tiles)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_20.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.horizontalLayout_20)
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.horizontalLayout_19 = QtGui.QHBoxLayout()
        self.horizontalLayout_19.setObjectName(_fromUtf8("horizontalLayout_19"))
        self.horizontalLayout_21 = QtGui.QHBoxLayout()
        self.horizontalLayout_21.setObjectName(_fromUtf8("horizontalLayout_21"))
        self.label_tilew = QtGui.QLabel(self.tab_3)
        self.label_tilew.setObjectName(_fromUtf8("label_tilew"))
        self.horizontalLayout_21.addWidget(self.label_tilew)
        self.tilew = QtGui.QLineEdit(self.tab_3)
        self.tilew.setObjectName(_fromUtf8("tilew"))
        self.horizontalLayout_21.addWidget(self.tilew)
        self.horizontalLayout_19.addLayout(self.horizontalLayout_21)
        self.horizontalLayout_22 = QtGui.QHBoxLayout()
        self.horizontalLayout_22.setObjectName(_fromUtf8("horizontalLayout_22"))
        self.label_tileh = QtGui.QLabel(self.tab_3)
        self.label_tileh.setObjectName(_fromUtf8("label_tileh"))
        self.horizontalLayout_22.addWidget(self.label_tileh)
        self.tileh = QtGui.QLineEdit(self.tab_3)
        self.tileh.setObjectName(_fromUtf8("tileh"))
        self.horizontalLayout_22.addWidget(self.tileh)
        self.horizontalLayout_19.addLayout(self.horizontalLayout_22)
        self.gridLayout_2.addLayout(self.horizontalLayout_19, 2, 0, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout_2)
        self.horizontalLayout_23 = QtGui.QHBoxLayout()
        self.horizontalLayout_23.setObjectName(_fromUtf8("horizontalLayout_23"))
        self.label_java = QtGui.QLabel(self.tab_3)
        self.label_java.setObjectName(_fromUtf8("label_java"))
        self.horizontalLayout_23.addWidget(self.label_java)
        self.java = QtGui.QLineEdit(self.tab_3)
        self.java.setObjectName(_fromUtf8("java"))
        self.horizontalLayout_23.addWidget(self.java)
        self.brows_java = QtGui.QPushButton(self.tab_3)
        self.brows_java.setObjectName(_fromUtf8("brows_java"))
        self.horizontalLayout_23.addWidget(self.brows_java)
        self.verticalLayout_2.addLayout(self.horizontalLayout_23)
        self.verticalLayout_4.addLayout(self.verticalLayout_2)
        self.tabWidget.addTab(self.tab_3, _fromUtf8(""))
        self.tab_4 = QtGui.QWidget()
        self.tab_4.setObjectName(_fromUtf8("tab_4"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.tab_4)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.horizontalLayout_18 = QtGui.QHBoxLayout()
        self.horizontalLayout_18.setObjectName(_fromUtf8("horizontalLayout_18"))
        self.metadata = QtGui.QCheckBox(self.tab_4)
        self.metadata.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.metadata.setChecked(False)
        self.metadata.setObjectName(_fromUtf8("metadata"))
        self.horizontalLayout_18.addWidget(self.metadata)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_18.addItem(spacerItem1)
        self.verticalLayout_5.addLayout(self.horizontalLayout_18)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        self.label_author = QtGui.QLabel(self.tab_4)
        self.label_author.setObjectName(_fromUtf8("label_author"))
        self.horizontalLayout_9.addWidget(self.label_author)
        self.author = QtGui.QLineEdit(self.tab_4)
        self.author.setObjectName(_fromUtf8("author"))
        self.horizontalLayout_9.addWidget(self.author)
        self.verticalLayout_5.addLayout(self.horizontalLayout_9)
        self.horizontalLayout_12 = QtGui.QHBoxLayout()
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        self.label_creator = QtGui.QLabel(self.tab_4)
        self.label_creator.setObjectName(_fromUtf8("label_creator"))
        self.horizontalLayout_12.addWidget(self.label_creator)
        self.creator = QtGui.QLineEdit(self.tab_4)
        self.creator.setObjectName(_fromUtf8("creator"))
        self.horizontalLayout_12.addWidget(self.creator)
        self.verticalLayout_5.addLayout(self.horizontalLayout_12)
        self.horizontalLayout_13 = QtGui.QHBoxLayout()
        self.horizontalLayout_13.setObjectName(_fromUtf8("horizontalLayout_13"))
        self.label_producer = QtGui.QLabel(self.tab_4)
        self.label_producer.setObjectName(_fromUtf8("label_producer"))
        self.horizontalLayout_13.addWidget(self.label_producer)
        self.producer = QtGui.QLineEdit(self.tab_4)
        self.producer.setObjectName(_fromUtf8("producer"))
        self.horizontalLayout_13.addWidget(self.producer)
        self.verticalLayout_5.addLayout(self.horizontalLayout_13)
        self.horizontalLayout_14 = QtGui.QHBoxLayout()
        self.horizontalLayout_14.setObjectName(_fromUtf8("horizontalLayout_14"))
        self.label_subject = QtGui.QLabel(self.tab_4)
        self.label_subject.setObjectName(_fromUtf8("label_subject"))
        self.horizontalLayout_14.addWidget(self.label_subject)
        self.subject = QtGui.QLineEdit(self.tab_4)
        self.subject.setObjectName(_fromUtf8("subject"))
        self.horizontalLayout_14.addWidget(self.subject)
        self.verticalLayout_5.addLayout(self.horizontalLayout_14)
        self.horizontalLayout_15 = QtGui.QHBoxLayout()
        self.horizontalLayout_15.setObjectName(_fromUtf8("horizontalLayout_15"))
        self.label_title = QtGui.QLabel(self.tab_4)
        self.label_title.setObjectName(_fromUtf8("label_title"))
        self.horizontalLayout_15.addWidget(self.label_title)
        self.title_out = QtGui.QLineEdit(self.tab_4)
        self.title_out.setObjectName(_fromUtf8("title_out"))
        self.horizontalLayout_15.addWidget(self.title_out)
        self.verticalLayout_5.addLayout(self.horizontalLayout_15)
        self.horizontalLayout_16 = QtGui.QHBoxLayout()
        self.horizontalLayout_16.setObjectName(_fromUtf8("horizontalLayout_16"))
        self.label_date = QtGui.QLabel(self.tab_4)
        self.label_date.setObjectName(_fromUtf8("label_date"))
        self.horizontalLayout_16.addWidget(self.label_date)
        self.date = QtGui.QDateTimeEdit(self.tab_4)
        self.date.setObjectName(_fromUtf8("date"))
        self.horizontalLayout_16.addWidget(self.date)
        self.verticalLayout_5.addLayout(self.horizontalLayout_16)
        self.horizontalLayout_17 = QtGui.QHBoxLayout()
        self.horizontalLayout_17.setObjectName(_fromUtf8("horizontalLayout_17"))
        self.label_keywords = QtGui.QLabel(self.tab_4)
        self.label_keywords.setObjectName(_fromUtf8("label_keywords"))
        self.horizontalLayout_17.addWidget(self.label_keywords)
        self.keywords = QtGui.QLineEdit(self.tab_4)
        self.keywords.setObjectName(_fromUtf8("keywords"))
        self.horizontalLayout_17.addWidget(self.keywords)
        self.verticalLayout_5.addLayout(self.horizontalLayout_17)
        self.verticalLayout_6.addLayout(self.verticalLayout_5)
        self.tabWidget.addTab(self.tab_4, _fromUtf8(""))
        self.tab_5 = QtGui.QWidget()
        self.tab_5.setObjectName(_fromUtf8("tab_5"))
        self.gridLayout_4 = QtGui.QGridLayout(self.tab_5)
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.gridLayout_3 = QtGui.QGridLayout()
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.verticalLayout_8 = QtGui.QVBoxLayout()
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.label_margins = QtGui.QLabel(self.tab_5)
        self.label_margins.setObjectName(_fromUtf8("label_margins"))
        self.verticalLayout_8.addWidget(self.label_margins)
        self.margins = QtGui.QLineEdit(self.tab_5)
        self.margins.setObjectName(_fromUtf8("margins"))
        self.verticalLayout_8.addWidget(self.margins)
        self.addimages = QtGui.QPushButton(self.tab_5)
        self.addimages.setObjectName(_fromUtf8("addimages"))
        self.verticalLayout_8.addWidget(self.addimages)
        self.label_2 = QtGui.QLabel(self.tab_5)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout_8.addWidget(self.label_2)
        self.content_list = QtGui.QTableWidget(self.tab_5)
        self.content_list.setObjectName(_fromUtf8("content_list"))
        self.content_list.setColumnCount(4)
        self.content_list.setRowCount(0)
        item = QtGui.QTableWidgetItem()
        self.content_list.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.content_list.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.content_list.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.content_list.setHorizontalHeaderItem(3, item)
        self.verticalLayout_8.addWidget(self.content_list)
        self.gridLayout_3.addLayout(self.verticalLayout_8, 0, 2, 1, 2)
        self.verticalLayout_7 = QtGui.QVBoxLayout()
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.topleft = QtGui.QRadioButton(self.tab_5)
        self.topleft.setObjectName(_fromUtf8("topleft"))
        self.verticalLayout_7.addWidget(self.topleft)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_7.addItem(spacerItem2)
        self.middleleft = QtGui.QRadioButton(self.tab_5)
        self.middleleft.setObjectName(_fromUtf8("middleleft"))
        self.verticalLayout_7.addWidget(self.middleleft)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_7.addItem(spacerItem3)
        self.bottomleft = QtGui.QRadioButton(self.tab_5)
        self.bottomleft.setObjectName(_fromUtf8("bottomleft"))
        self.verticalLayout_7.addWidget(self.bottomleft)
        self.gridLayout_3.addLayout(self.verticalLayout_7, 0, 15, 1, 1)
        self.verticalLayout_9 = QtGui.QVBoxLayout()
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))
        self.topright = QtGui.QRadioButton(self.tab_5)
        self.topright.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.topright.setObjectName(_fromUtf8("topright"))
        self.verticalLayout_9.addWidget(self.topright)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_9.addItem(spacerItem4)
        self.middleright = QtGui.QRadioButton(self.tab_5)
        self.middleright.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.middleright.setObjectName(_fromUtf8("middleright"))
        self.verticalLayout_9.addWidget(self.middleright)
        spacerItem5 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_9.addItem(spacerItem5)
        self.bottomright = QtGui.QRadioButton(self.tab_5)
        self.bottomright.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.bottomright.setObjectName(_fromUtf8("bottomright"))
        self.verticalLayout_9.addWidget(self.bottomright)
        self.gridLayout_3.addLayout(self.verticalLayout_9, 0, 16, 1, 1)
        self.gridLayout_4.addLayout(self.gridLayout_3, 0, 1, 1, 1)
        self.tabWidget.addTab(self.tab_5, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.verticalLayout_14 = QtGui.QVBoxLayout(self.tab_2)
        self.verticalLayout_14.setObjectName(_fromUtf8("verticalLayout_14"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.textBrowser = QtGui.QTextBrowser(self.tab_2)
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.horizontalLayout_5.addWidget(self.textBrowser)
        self.textBrowser_2 = QtGui.QTextBrowser(self.tab_2)
        self.textBrowser_2.setObjectName(_fromUtf8("textBrowser_2"))
        self.horizontalLayout_5.addWidget(self.textBrowser_2)
        self.verticalLayout_14.addLayout(self.horizontalLayout_5)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.verticalLayout_3.addWidget(self.tabWidget)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.progressBar = QtGui.QProgressBar(GeospatialPDF)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.horizontalLayout_2.addWidget(self.progressBar)
        self.buttonBox = QtGui.QDialogButtonBox(GeospatialPDF)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonBox.sizePolicy().hasHeightForWidth())
        self.buttonBox.setSizePolicy(sizePolicy)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Close|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.horizontalLayout_2.addWidget(self.buttonBox)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.retranslateUi(GeospatialPDF)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), GeospatialPDF.reject)
        QtCore.QMetaObject.connectSlotsByName(GeospatialPDF)

    def retranslateUi(self, GeospatialPDF):
        GeospatialPDF.setWindowTitle(_translate("GeospatialPDF", "GeospatialPDF export", None))
        self.title.setText(_translate("GeospatialPDF", "GeospatialPDF export", None))
        self.subtitle.setText(_translate("GeospatialPDF", "Export your active layers in GeospatialPDF format", None))
        self.label_ogr.setText(_translate("GeospatialPDF", "OGR active layers :", None))
        self.label_gdal.setText(_translate("GeospatialPDF", "GDAL active layers :", None))
        self.label_out.setText(_translate("GeospatialPDF", "Output file", None))
        self.browseButton.setText(_translate("GeospatialPDF", "Browse", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("GeospatialPDF", "Export", None))
        self.label_3.setText(_translate("GeospatialPDF", "Select projection system", None))
        self.label_compr.setText(_translate("GeospatialPDF", "Select compression algorithm", None))
        self.compr.setItemText(0, _translate("GeospatialPDF", "Deflate", None))
        self.compr.setItemText(1, _translate("GeospatialPDF", "None", None))
        self.compr.setItemText(2, _translate("GeospatialPDF", "JPEG", None))
        self.compr.setItemText(3, _translate("GeospatialPDF", "JPEG2000", None))
        self.label_dpi.setText(_translate("GeospatialPDF", "DPI to use", None))
        self.dpi.setText(_translate("GeospatialPDF", "72", None))
        self.label_extent.setText(_translate("GeospatialPDF", "<html><head/><body><p>Clipping extent</p><p><span style=\" font-size:7pt; font-style:italic;\">(xmin,ymin,xmax,ymax)</span></p></body></html>", None))
        self.bbox.setText(_translate("GeospatialPDF", "QGIS bounding box", None))
        self.tiles.setText(_translate("GeospatialPDF", "Create tiled PDF file", None))
        self.label_tilew.setText(_translate("GeospatialPDF", "Tile width", None))
        self.label_tileh.setText(_translate("GeospatialPDF", "Tile height", None))
        self.label_java.setText(_translate("GeospatialPDF", "<html><head/><body><p><a href=\"http://partners.adobe.com/public/developer/en/acrobat/sdk/AcroJS.pdf\"><span style=\" text-decoration: underline; color:#0057ae;\">Javascript file</span></a> to embed and<br/>run at document opening.</p></body></html>", None))
        self.brows_java.setText(_translate("GeospatialPDF", "Browse", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("GeospatialPDF", "Options", None))
        self.metadata.setText(_translate("GeospatialPDF", "Write creation metadata information into output PDF", None))
        self.label_author.setText(_translate("GeospatialPDF", "Author", None))
        self.label_creator.setText(_translate("GeospatialPDF", "Creator", None))
        self.label_producer.setText(_translate("GeospatialPDF", "Producer", None))
        self.label_subject.setText(_translate("GeospatialPDF", "Subject", None))
        self.label_title.setText(_translate("GeospatialPDF", "Title", None))
        self.label_date.setText(_translate("GeospatialPDF", "Creation date", None))
        self.label_keywords.setText(_translate("GeospatialPDF", "Keywords", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), _translate("GeospatialPDF", "Author", None))
        self.label_margins.setText(_translate("GeospatialPDF", "<html><head/><body><p>Margins (<span style=\" font-size:7pt; font-style:italic;\">left,right,top,bottom</span>)</p></body></html>", None))
        self.addimages.setText(_translate("GeospatialPDF", "Add elements (legend, logos, etc)", None))
        self.label_2.setText(_translate("GeospatialPDF", "The elements should be images", None))
        item = self.content_list.horizontalHeaderItem(0)
        item.setText(_translate("GeospatialPDF", "Path", None))
        item = self.content_list.horizontalHeaderItem(1)
        item.setText(_translate("GeospatialPDF", "Scale", None))
        item = self.content_list.horizontalHeaderItem(2)
        item.setText(_translate("GeospatialPDF", "Position", None))
        item = self.content_list.horizontalHeaderItem(3)
        item.setText(_translate("GeospatialPDF", "Link", None))
        self.topleft.setText(_translate("GeospatialPDF", "Top-left", None))
        self.middleleft.setText(_translate("GeospatialPDF", "Middle-left", None))
        self.bottomleft.setText(_translate("GeospatialPDF", "Bottom-left", None))
        self.topright.setText(_translate("GeospatialPDF", "Top-right", None))
        self.middleright.setText(_translate("GeospatialPDF", "Middle-right", None))
        self.bottomright.setText(_translate("GeospatialPDF", "Bottom-right", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_5), _translate("GeospatialPDF", "Layout", None))
        self.textBrowser.setHtml(_translate("GeospatialPDF", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Cantarell\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">This plugin permit to export the active layers to </span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt; font-weight:600;\">GeospatialPDF</span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\"> maintaining the style of vector layers (no all style features of QGIS are supported, only some). It support both raster and vector layers</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:12pt; text-decoration: underline;\">GeospatialPDF format</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:12pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">DPI value is used only when raster is present during conversion. DPI is used to modify the dimensions of output file, increasing the DPI value you will obtain a PDF with smaller width and height.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">For more information read the </span><a href=\"http://www.gdal.org/frmt_pdf.html\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt; text-decoration: underline; color:#0057ae;\">GeospatialPDF format</span></a><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\"> page. </span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:12pt; text-decoration: underline;\">How to use</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">Activate the layers that you want to export into QGIS canvas, only checked layers will be exported. If no layers are selected an error will be reported.</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">Launch GeospatialPDF export plugin and set the output name for the output file.</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">Now you can fill all the optional parameters in the </span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt; font-style:italic;\">Options</span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\"> and </span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt; font-style:italic;\">Author</span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\"> tabs. </span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">In the </span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt; font-style:italic;\">Layout</span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\"> tab you can manage the layout of resulting GeospatialPDF, the user can set margins or add legend, north arrow or anything else useful</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:12pt; text-decoration: underline;\">Troubleshooting</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">Problems could appear with PostGIS layer having to geometry columns, right now is not able to select the right column</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">Not all the QGIS style capabilities are supported by </span><a href=\"http://www.gdal.org/ogr_feature_style.html\"><span style=\" text-decoration: underline; color:#0000ff;\">OGR Style Feature</span></a><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">.</span></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p align=\"justify\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p></body></html>", None))
        self.textBrowser_2.setHtml(_translate("GeospatialPDF", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Cantarell\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">Developed by Luca Delucchi, Fondazione Edmund Mach, San Michele all\'Adige, Trento, Italy</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'DejaVu Sans\'; font-size:9pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">Plugin financed by </span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt; font-weight:600;\">Provincia Autonoma di Trento</span><span style=\" font-family:\'DejaVu Sans\'; font-size:9pt;\">, </span><span style=\" font-family:\'Verdana,Arial,Helvetica,sans-serif\'; font-size:9pt; font-style:italic; color:#000000;\">Dipartimento Territorio, Ambiente e Foreste Servizio Autorizzazioni e Valutazioni Ambientali, Ufficio Sistemi Informativi</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("GeospatialPDF", "Help", None))

import gpdf_rc
